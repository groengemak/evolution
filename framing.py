#!/usr/bin/env python3
#
# Framing -- is it possible to avoid framing errors?
#
# DNA consists of codons, and it matters how they are framed.
# There are 2 directions, and 3 start points for each.
#
# If we weed out wrongful START codings, can we still encode
# for all amino acids?
#
# From: Rick van Rein <rick@groengemak.nl>


#import 


# DNA information (for humans; bacteria have more starts)
start = [ 'AUG' ]
stop = [ 'UAA', 'UAG', 'UGA' ]
amino2codons = {
    'Met': [ 'AUG' ],
    'Thr': [ 'ACU', 'ACC', 'ACA', 'ACG' ],
    'Asn': [ 'AAU', 'AAC' ],
    'Lys': [ 'AAA', 'AAG' ],
    'Ser': [ 'AGU', 'AGC', 'UCU', 'UCC', 'UCA', 'UCG' ],
    'Arg': [ 'AGA', 'AGG', 'CGU', 'CGC', 'CGA', 'CGG' ],
    'Val': [ 'GUU', 'GUC', 'GUA', 'GUG' ],
    'Ala': [ 'GCU', 'GCC', 'GCA', 'GCG' ],
    'Asp': [ 'GAU', 'GAC' ],
    'Glu': [ 'GAA', 'GAG' ],
    'Gly': [ 'GGY', 'GGC', 'GGA', 'GGG' ],
    'Phe': [ 'UUU', 'UUC' ],
    'Leu': [ 'UUA', 'UUG', 'CUU', 'CUC', 'CUA', 'CUG' ],
    'Tyr': [ 'UAU', 'UAC' ],
    'Cys': [ 'UGU', 'UGC' ],
    'Trp': [ 'UGG' ],
    'Pro': [ 'CCU', 'CCC', 'CCA', 'CCG' ],
    'His': [ 'CAU', 'CAC' ],
    'Gln': [ 'CAA', 'CAG' ],
    'Ile': [ 'AUU', 'AUC', 'AUA' ]
    }

# Derviations
codons = stop
for k in amino2codons:
    codons += amino2codons [k]
codon2amino = { }
for a in amino2codons:
    for c in amino2codons [a]:
        codon2amino [c] = a

# Correctness checks on the input
for i in range (len (codons)):
    assert codons [i] not in codons [i+1:], "Double occurrence of %s" % (codons [i])
assert len (amino2codons) == 20, "#amino-acids == %d != 20" % len (amino2codons)
assert len (codons) == 64, "#codons == %d != 64" % len (codons)
assert len (set (codons)) == 64, "#unique(codons) == %d != 64" % len (set (codons))

# Can we encode anything without spurious starts?
permissible = set (codons)
for s in start:
    for letter in [ 'A', 'C', 'U', 'G' ]:
        togo = '%c%s' % (letter, s [:2])
        permissible.remove (togo)
expressible = { codon2amino [c] for c in permissible if c in codon2amino }
print ("Expressed aminos: %r" % expressible)
assert len (expressible) == 20, "Can only express %d out of 20 amino acids" % (len (expressible))
