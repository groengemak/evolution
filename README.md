# Scripting Evolution

> *Experiments and tests with evolution, mutation and niche selection.*

**framing.py** tries to figure out if DNA can be coded in such a manner
	that spurious START codons are avoided, while still allowing
	all proteins to be coded.

